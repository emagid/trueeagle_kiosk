<?php

use Model\Product;
use Model\Product_Map;

class siteController extends \Emagid\Mvc\Controller {
	
	protected $viewData;
	protected $productMap;
	function __construct(){
		parent::__construct();

		$this->productMap = new Product_Map();
		$productMap = new Product_Map();
		$this->configs = \Model\Config::getItems();
		$cart = new stdClass();
		$cart->credits = [];
		$cart->packages = [];
		$cart->products = [];
		$cart->total = 0;

		$wishList = new stdClass();
		$wishList->credits = [];
		$wishList->packages = [];
		$wishList->products = [];
		$wishList->total = 0;
		$combo = new stdClass();

		$this->viewData = (object)[
			'pages' => \Model\Page::getList(),
			'services' => \Model\Service::getList(['orderBy'=>'display_order asc, name asc']),
			'cart' => $cart,
			'wishlist' => $wishList,
			'user' => null,
			'combo' => $combo,
			'coupon' => \Model\Coupon::getItem(null,['where'=>"num_uses_all::integer > 0"]), //change in the future to randomize?
		];

		if(\Emagid\Core\Membership::isAuthenticated() && (\Emagid\Core\Membership::isInRoles(['customer']) || \Emagid\Core\Membership::isInRoles(['provider'])) || \Emagid\Core\Membership::isInRoles(['wholesale']) ){
			if (\Emagid\Core\Membership::isInRoles(['provider'])){
				$this->viewData->user = \Model\Provider::getItem(\Emagid\Core\Membership::userId());
			}

			if (\Emagid\Core\Membership::isInRoles(['customer'])){
				$this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
			}

			if (\Emagid\Core\Membership::isInRoles(['wholesale'])){
				$this->viewData->user = \Model\User::getItem(\Emagid\Core\Membership::userId());
			}
		}

		$_offices = \Model\Office::getList();
		$offices = [];
		foreach($_offices as $office){
			if(!isset($offices[$office->practice()->name])) $offices[$office->practice()->name]=[];
			$offices[$office->practice()->name][$office->id] = $office;
		}
		$this->viewData->book_offices = $offices;
	}
	
	public function index(Array $params = []){
		$this->loadView($this->viewData);
	}

	private function validateCookie($cookie)
	{
		$required = ['product_id', 'product_type'];
		foreach($cookie as $key => $data) {
			foreach ($required as $index) {
				if (!isset($data[$index]) || !$data[$index]) {
					return false;
				}
			}
		}

		return true;
	}

	public function toJson($array)
	{
		header('Content-Type: application/json');
		echo json_encode($array);
		exit();
	}
}