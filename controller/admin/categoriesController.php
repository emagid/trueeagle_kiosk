<?php

class categoriesController extends adminController {
	
	function __construct(){
		parent::__construct("Category", "categories");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

		$params['queryOptions']['orderBy'] = 'display_order asc, name';

		parent::index($params);
	}

	function update(Array $arr = []){
		$cat = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->categories = \Model\Category::getList(['where'=>"id<>".$cat->id." AND active = 1"]);
		$productsWhere = ' product.active = 1 and product.id in ';
		$productsWhere .= ' ( select pc.product_id from product_categories pc where pc.category_id = '.$cat->id.' and pc.active = 1 ) ';
		$this->_viewData->products = \Model\Product::getList(['where'=>$productsWhere]);

		parent::update($arr);
	}
  
}