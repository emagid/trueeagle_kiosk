<?php

namespace Model;

use Emagid\Core\Model;

class Wholesale extends Model{
    static $tablename = 'wholesale';
    static $fields = [
        'company',
        'job_title',
        'dba',
        'address',
        'address2',
        'incorporation',
        'tax_id',
        'referrer',
        'phone',
        'alt_phone',
        'status' //0->pending, 1->approved, 2->rejected

    ];
}