<?php

namespace Model;

class Car_Signup extends \Emagid\Core\Model {
    static $tablename = "public.popshap";

    public static $fields  =  [
        'name',
        'email',
    ];
}