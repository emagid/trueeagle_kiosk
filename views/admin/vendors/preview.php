<div class = "content_wrapper" id = "wrapper">

    <div class = "dashboard_wrapper">

        <div class = "dashboard_main main_dashboard">

            <div class = "col_left">
                <div class = "img_cropper user_photo_wrapper">
                    <div class = "image_upload_button">
                        <div class = "camera_icon"></div>
                        <div class = "camera_trigger_text">Update Profile Picture</div>
                        
                    </div>
                    <div class = "user_image" style = "background-image:url(/content/uploads/providers/<?  $img=$this->viewData->user->photo; 
                    if(!empty($img)){echo $img;} else{echo 'no-image.png';}?>);" class = "user_photo"></div>
                </div>

                <div class = "user_details">
                    <h2 class = "name"><?=$this->viewData->user->first_name?> <?=$this->viewData->user->last_name?></h2>
                    <span class = "location"><?=$model->location;?></span>
                </div>

                <div class = "profile_progress">
                    <p class = "utopia_tip">
                    Profile Progress
                    </p>
                    <div class = "profile_progress_meter">
                        <div class = "profile_progress_border">
                        </div>
                        <div class = "profile_progress_level">
                            <p class = "profile_progress_percent">75%</p>
                        </div>
                    </div>
                    <div class = "complete_profile_button" id = "show_complete_profile_view">
                        <p>Complete Profile</p>
                    </div>
                </div>

                <div class = "user_dashboard_tabs">
                    <ul class = "vertical_list">
                        <li class = "user_dashboard_tab user_dashboard_tab_active">
                            <p>Dashboard</p>
                        </li>
                        <li class = "user_dashboard_tab">
                            <p>Session History</p>
                            <a class = "tab_button">View Full</a>
                        </li>
                        <li class = "user_dashboard_tab">
                            <p>Packages</p>
<!--                                 <span class = "user_dashboard_tab_notification">
                                <p class = "notification_count">1</p>
                            </span> -->
                        </li>
                        <li class = "user_dashboard_tab">
                            <p>Your Payments</p>
                        </li>
                        <li class = "user_dashboard_tab">
                            <p>Inbox</p>
                        </li>
                    </ul>
                </div>
                
                <div class = "user_activity_trigger_section">
                    <div class = "medium_text">
                        Take the health questionnaire
                    </div>
                    <span class = "user_activity_right_arrow">
                    </span>
                </div>

            </div>

            <div class = "col_right">
                <div class = "container_with_header full_width dashboard_calendar">
                    <div class = "header">
                        <h2 class = "header_text">This Week</h2>
                    </div>
                    <div class = "this_week_box">
                        <h2>Welcome, <b><?=$this->viewData->user->first_name?> <?=$this->viewData->user->last_name?></b>. How you doing?</b>
                    </div>
                </div>
                <div class = "container_with_header full_width dashboard_calendar">
                    <div class = "header">
                        <h2 class = "header_text">Calendar</h2>
                        <span><p>Manage and view Calendar</p></span>
                        <div class = "right">   
                            <div class = "sessions_pending_indicator">
                                <p>Sessions Pending</p>
                                <span class = "circle_notification">
                                    <p class = "sessions_pending_notification_number">1</p>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class = "dashboard_calendar_container">
                        <div class = "provider_dashboard_small_calendar_box_wrapper calendar_box_wrapper">
                            <div class = "calendar_selector_header">
                                <div class = "header_selector_dates">
                                    <div class = "cal_header_tile">
                                        <div class = "date_collection" id = "big_cal_header_dashboard">

                                            <div class = "cal_header_date cal_header_date_current">
                                                <div class = "cal_header_date_name">
                                                Monday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 20, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_current">
                                                <div class = "cal_header_date_name">
                                                Tuesday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 21, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_current">
                                                <div class = "cal_header_date_name">
                                                Wednesday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 22, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_current">
                                                <div class = "cal_header_date_name">
                                                Thursday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 23, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_current">
                                                <div class = "cal_header_date_name">
                                                Friday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 24, 2015
                                                </div>
                                            </div>


                                            <div class = "cal_header_date cal_header_date_next">
                                                <div class = "cal_header_date_name">
                                                Monday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 27, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_next">
                                                <div class = "cal_header_date_name">
                                                Tuesday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 28, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_next">
                                                <div class = "cal_header_date_name">
                                                Wednesday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 29, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_next">
                                                <div class = "cal_header_date_name">
                                                Thursday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 30, 2015
                                                </div>
                                            </div>
                                            <div class = "cal_header_date cal_header_date_next">
                                                <div class = "cal_header_date_name">
                                                Friday
                                                </div>
                                                <div class = "cal_header_date_value">
                                                July 31, 2015
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class = "control">
                                    <div class = "control_prev control_prev_disabled opacity_ease" data-control_calendar = "times_grid_previous"></div>
                                    <div class = "control_next opacity_ease" data-control_calendar = "times_grid_next"></div>
                                </div>
                            </div>

                            <div class = "time_tile_collection_viewport">
                                <div class = "time_column">
                                    <div class = "calendar_management_block management_block_unavailable" data-cal_block_type = "unavailable">
                                        <div class = "calendar_management_text">
                                            <p>Adjust your availability on this day</p>
                                        </div>
                                    </div>
                                </div>
                                <div class = "time_column">
                                    <div class = "calendar_management_block management_block_approved_session" data-cal_block_type = "approved">
                                        <p class = "session_status_indicator">Confirmed</p>
                                        <div class = "calendar_management_text">
                                            <p><b>2:30 pm</b> with</p>
                                            <p>Client First Last</p>
                                        </div>
                                    </div>
                                    <div class = "calendar_management_block management_block_pending_session" data-cal_block_type = "pending">
                                        <p class = "session_status_indicator pending_session_indicator">Pending</p>
                                        <div class = "calendar_management_text">
                                            <p><b>2:30 pm</b> with</p>
                                            <p>Client First Last</p>
                                        </div>
                                    </div>
                                </div>
                                <div class = "time_column">
                                    <div class = "calendar_management_block management_block_available" data-cal_block_type = "available">
                                        <div class = "calendar_management_text">
                                            <p>Nothing Booked Yet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class = "time_column">
                                    <div class = "calendar_management_block management_block_approved_session" data-cal_block_type = "approved">
                                        <p class = "session_status_indicator">Confirmed</p>
                                        <div class = "calendar_management_text">
                                            <p><b>2:30 pm</b> with</p>
                                            <p>Client First Last</p>
                                        </div>
                                    </div>
                                    <div class = "calendar_management_block management_block_approved_session" data-cal_block_type = "approved">
                                        <p class = "session_status_indicator">Confirmed</p>
                                        <div class = "calendar_management_text">
                                            <p><b>2:30 pm</b> with</p>
                                            <p>Client First Last</p>
                                        </div>
                                    </div>
                                </div>
                                <div class = "time_column">
                                    <div class = "calendar_management_block management_block_available" data-cal_block_type = "available">
                                        <div class = "calendar_management_text">
                                            <p>Nothing Booked Yet</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>          
                        <div class = "provider_dashboard_cal_popout cal_management_popout" id = "cal_management_popout">
                            <div class = "cal_management_popout_container">
                                <div class = "cal_management_pending_session_wrapper cal_management_popout_container_visible_section">
                                    <h1 class = "cal_popout_title">Pending Session</h1>
                                    <div class = "cal_popout_button_wrapper accept_reject_session two_prong_button_wrapper">
                                        <a class = "accept_reject_button accept_button two_prong_button">Accept</a>
                                        <a class = "accept_reject_button reject_button two_prong_button">Reject</a>
                                        <div class = "accept_reject_vertical_separator two_prong_button_vert_separator"></div>
                                    </div>
                                    <div class = "cal_popout_options">
                                        <a class = "extend_cal_popout cal_popout_option opacity_ease" data-extend_popout_type = "reschedule" id = "popout_suggest_alternate_time">Suggest an alternate time</a>  
                                        <a class = "extend_cal_popout cal_popout_option opacity_ease" data-extend_popout_type = "client_info" id = "popout_view_client_info">View <span>First's</span> Info</a>
                                        <a class = "cal_popout_option opacity_ease"id = "popout_view_client_message">Send a Message</a>
                                    </div>
                                    
                                </div>
                                <div class = "cal_management_popout_container_hidden_sections">
                                    <div class = "horizontal_separator">
                                    </div>
                                    <div class = "reschedule cal_management_popout_container_hidden_section">
                                        <p>Enter a new date and time</p>
                                        <div class = "reschedule_form_wrapper">
                                            <form>
                                                <input type="text" name="when" id="datepicker_reschedule" value="" placeholder="Select a date">
                                                <i class = "calendar_icon"></i>
                                                <input type="text" name="time" id="timepicker_reschedule" value="" placeholder="Select a time" class="hasTimepicker">
                                                <i class = "time_icon"></i>
                                                <div id = "submit_reschedule_datetime">
                                                    <p>Reschedule Session</p>
                                                </div>
                                            </form>
                                        </div>
                                        <p class = "receipt_message">Your reschedule request has been sent over to First.</p>
                                    </div>
                                    <div class = "client_info cal_management_popout_container_hidden_section">
                                        <div class = "client_info_row">
                                            <div class = "user_image">

                                            </div>
                                            <div class = "client_info_name_location"> 
                                                <h6>First + Last</h6>
                                                <p class = "location">East Village, NY</p>
                                            </div>
                                        </div>
                                        <div class = "client_info_blackarea">
                                            <div class = "client_info_row">
                                                <p>Age:</p>
                                                <b>23 years old</b>
                                            </div>
                                            <div class = "client_info_row">
                                                <p>Interests:</p>
                                                <div class = "interests_list">
                                                    <b>Yoga</b>
                                                    <b>Meditation</b>
                                                    <b>Fitness</b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class = "cal_popout_arrow">
                                </div>
                                <div class = "cal_popout_arrow cal_popout_arrow_outline">
                                </div>
                            </div>
                        </div>              
                    </div>
                </div>
              <!--   <div class = "container_with_header full_width">
                    <div class = "header">
                        <h2 class = "header_text">New Packages</h2>
                        <span class = "container_header_alert"><p>1</p></span>
                        <div class = "right">   
                            <div class = "mark_as_seen">
                                <p>Mark as seen</p>
                                <span>
                                    <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                                        <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>

                    
                    <div class = "packages_list_wrapper packages_provider_perspective">
                        <div class = "package_container">
                            <div class = "package_summary_square">
                                <h1>5x</h1>
                                <p>yoga</p>
                            </div>
                            <div class = "package_user package_client_name">
                                <span>Jacob L.</span>
                                <span class = "package_receipts">$800 Purchased at 3:30 PM, July 1 2015</span>
                            </div>
                            
                            <div class = "remaining_sessions many_remaining">
                                <h1><span class = "remaining_count">4</span>/<span class = "package_size">5</span></h1>
                                <p>Remaining</p>
                            </div>
                            <div class = "fl_right">
                                <div class = "package_expiration">
                                    <p class = "expires_date">
                                    Expires <span>Aug 31, 2015</span>
                                    </p>
                                    <p class = "purchase_date">
                                    Purchased <span>July 1, 2015</span>
                                    </p>
                                </div>  
                            </div>
                        </div>

                    </div>
                </div> -->






 <div class = "container_with_header full_width">
                    <div class = "header">
                        <h2 class = "header_text">All active packages</h2>
                       <!--  <span class = "container_header_alert"><p>1</p></span> -->
                        <div class = "right">   
                            <div class = "mark_as_seen">
                                <p>Mark as seen</p>
                                <span>
                                    <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                                        <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>

                    
                 <div class = "packages_list_wrapper packages_provider_perspective">
                         <?php foreach($this->viewData->packages as $packages) { ?>
                         <div class = "package_container">
                            <div class = "package_summary_square">
                                <h1>x<?=$packages->quantity?></h1>
                                <p><?=$packages->name?></p>
                            </div>
                            <div class = "package_user package_client_name">
                                <span><?=$packages->name?></span>
                                <span class = "package_receipts">$<?=$packages->price?> for <?=$packages->quantity?> days</span>
                            </div>
                            
                            <div class = "remaining_sessions many_remaining">
                                <h1><span class = "remaining_count"><?=$packages->expiration?></span></h1>
                                <p>&nbsp; &nbsp;  days </p>
                            </div>
                            <div class = "fl_right">
                                <div class = "package_expiration">
                                   <!--  <p class = "expires_date">
                                    Expires <span>Aug 31, 2015</span>
                                    </p> -->
                                    <p class = "purchase_date">
                                    Created <span><?echo $packages->insert_time?></span>
                                    </p>
                                </div>  
                            </div>
                        </div>
                          <?}?>
                    </div>
                </div>











                <div class = "container_with_header full_width">
                    <div class = "header">
                        <h2 class = "header_text">Active Packages</h2>

                    </div>
                    <div class = "packages_list_wrapper packages_provider_perspective">
                        <div class = "package_container">
                            <div class = "package_summary_square">
                                <h1>10x</h1>
                                <p>meditation</p>
                            </div>
                            <div class = "package_user package_client_name">
                                <span>Hannah R.</span>
                                <span class = "package_receipts">$1600 Purchased at 3:30 PM, July 1 2015</span>
                            </div>
                            
                            <div class = "remaining_sessions">
                                <h1><span class = "remaining_count">1</span>/<span class = "package_size">10</span></h1>
                                <p>Remaining</p>
                            </div>
                            <div class = "fl_right">
                                <div class = "package_expiration">
                                    <p class = "expires_date">
                                    Expires <span>Aug 31, 2015</span>
                                    </p>
                                    <p class = "purchase_date">
                                    Purchased <span>July 1, 2015</span>
                                    </p>
                                </div>  
                            </div>
                        </div>
                        <div class = "package_container">
                            <div class = "package_summary_square">
                                <h1>5x</h1>
                                <p>yoga</p>
                            </div>
                            <div class = "package_user package_client_name">
                                <span>Johnny H.</span>
                                <span class = "package_receipts">$800 Purchased at 3:30 PM, July 1 2015</span>
                            </div>
                            
                            <div class = "remaining_sessions">
                                <h1><span class = "remaining_count">2</span>/<span class = "package_size">5</span></h1>
                                <p>Remaining</p>
                            </div>
                            <div class = "fl_right">
                                <div class = "package_expiration">
                                    <p class = "expires_date">
                                    Expires <span>Aug 31, 2015</span>
                                    </p>
                                    <p class = "purchase_date">
                                    Purchased <span>July 1, 2015</span>
                                    </p>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class = "container_with_header full_width">
                    <div class = "header">
                        <h2 class = "header_text">Inbox</h2>
                        <span class = "container_header_alert"><p>1</p></span>
                        <div class = "right">   
                            <div class = "mark_as_seen">
                                <p>Mark all as read</p>
                                <span>
                                    <svg class="modal_svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="32" height="32" viewBox="0 0 32 32">
                                        <path class="modal_svg_path" d="M31.158 32.118c-.246 0-.492-.093-.678-.282L.163 1.52c-.375-.375-.375-.983 0-1.358s.983-.375 1.358 0l30.317 30.316c.375.375.375.983 0 1.358-.187.188-.433.282-.678.282zM.842 32.118c-.246 0-.492-.093-.678-.282-.375-.375-.375-.983 0-1.358L30.48.162c.375-.375.983-.375 1.358 0s.375.983 0 1.358L1.52 31.836c-.186.188-.432.282-.677.282z" fill="#fff"></path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class = "inbox_list_wrapper">
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                        <div class = "inbox_message_wrapper">
                            <div class = "unread_message_indicator">
                            </div>
                            <div class = "inbox_message_from_user_photo">
                            </div>
                            <div class = "inbox_message_from_user_text">
                                <div class = "inbox_message_from_user_name">
                                    <p>First Last</p>
                                    <p class = "inbox_message_datetime">3:30 PM July 31</p>
                                </div>
                                <div class = "inbox_message_text_body">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                            <div class = "inbox_message_actions_wrapper">
                                <div id = "expand_inbox_message">
                                    <i class = "chevron_down">
                                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="-113.2 143.3 24.4 15" enable-background="new -113.2 143.3 24.4 15" xml:space="preserve">
                                            <path d="M-109.9,143.7l8.9,8.9c0,0,0,0,0,0l8.9-8.9c0.1-0.1,0.3-0.1,0.5,0l2.4,2.4c0.1,0.1,0.1,0.3,0,0.5l-11.5,11.5
                                                c-0.1,0.1-0.3,0.1-0.5,0l-11.5-11.5c-0.1-0.1-0.1-0.3,0-0.5l2.4-2.4C-110.3,143.5-110.1,143.5-109.9,143.7z"/>
                                        </svg>

                                    </i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div>



    </div>

</div>