<!-- Modals -->

<!-- login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Log In</h4>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="loginemail" class="sr-only">Email address</label>
						<input type="email" class="form-control" id="loginemail" placeholder="Email address *">
					</div>
					<div class="form-group">
						<label for="loginpass" class="sr-only">Password</label>
						<input type="password" class="form-control" id="loginpass" placeholder="Password *">
					</div>
					<div class="form-group">
						<div class="pull-left">
							<label>
								<input type="checkbox"> Remember Me
							</label>
						</div>
						<div class="pull-right">
							<a href="#">Forgot your password?</a>
						</div>
					</div>
					<br /><br /><br />
					<div class="form-group">
						<a href="myaccount.php" class="btn btn-primary btn-block">Log In</a>
					</div>
					<a href="#" class="btn btn-default btn-block">Create Account</a>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- newsletter -->
<div class="modal fade" id="newsletter" tabindex="-1" role="dialog" aria-labelledby="newsletter" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Sign up & get exclusive offers</h4>
				<hr />
			</div>
			<div class="modal-body">

				<div class="inner">
					<p>Up To</p> 
					<span class="number">
						70
						<span class="right">
							<span class="top">%</span>
							<span class="bottom">Off</span>
						</span>
					</span>
				</div>
				<form>
					<div class="form-group">
						<label for="loginemail" class="sr-only">Email address</label>
						<input type="email" class="form-control" id="loginemail" placeholder="Email">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">Sign Up</button>
					</div>
					<div class="text-center">
						<a href="#" data-dismiss="modal" aria-label="Close"><em>No, Thank You</em></a>
						<br /><br /><br />
						<p> * By entering your email you are eligible to recieve promotional offers from ITMUSTBETIME.COM</p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- add to cart -->
<div class="modal fade" id="add-to-cart" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title">This item has been added to your cart!</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-10">
						<a href="cart.php" class="btn btn-primary btn-block">View Cart</a>
					</div>
					<div class="col-sm-14">
						<a href="#" class="btn btn-default btn-block" data-dismiss="modal">Continue Shopping</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<!-- wishlist -->
<div class="modal fade" id="wishlist" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h5 class="modal-title">This item has been added to your wishlist!</h5>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-10">
						<a href="myaccount-wishlist.php" class="btn btn-primary btn-block">View Wishlist</a>
					</div>
					<div class="col-sm-14">
						<a href="#" class="btn btn-default btn-block" data-dismiss="modal">Continue Shopping</a>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>